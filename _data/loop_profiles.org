
* ID notes 
** type suffix
- 1
- 2
- 3
- {digits,digits} + &: hybrid, digits in order of amount.
  - e.g. a type 2 hybrid with one type 1 loop is p8/2F7/&21
    
** p8/2F7/2
- 'p8' is global prefix to talk about patchloops
- '2' means type 2
** colors
- red, pink: 7
- checkerbox, gradient: 6
- green: 3
- orange: 9
- blue: 2
- white: 1
- black: 0
- quad, nib: 4
- yellow: 5
- brown, purple: 8
** constructing id
take three main attributes and form number
'green orange blue' is 392.  then convert that to base-18.  (like the loom axes).  Also, middle part of id is always three digits, even if 0 needing to pad number
* patches
** DONE blue and white 
   CLOSED: [2019-12-30 Mon 17:25]
*** id
 p8/0BE/2
 "blue white blue (bar)"
*** analysis
 A type 2 patch, arguably characterized by a distinct horizontal set foreground of darker blues.  The vertical set here is lighter, almost pastel tones with a large section of sequential blues spanning X6 to X14.  The foreground is composed of rich, marine blues, reds, greens, and yellows, in diminishing frequency respectively.  

 This patch is typically paired with ####, or the "red and black" checkerbox.  But while red and black creates a more consistent and even monochrome, this patch is rather successful in the opposite way: a sense of a balanced and clear checkerbox is brought about at almost the limit of granularity.  The biggest threat to the overall checkerbox gestalt is the two vertical pink loops at X2 and X16. 
*** DONE interpretation
CLOSED: [2019-12-24 Tue 16:17]
 As reflected in the above analysis, the large consensus concerning the understated sequence of blue, vertical loops in this patch goes that it runs exactly 8 loops, X6 to X14, but this has in fact been under debate for a long time.  Pinkston (1983) was the first to identify the sequence, affectionality referring to it as "the blue bar".  He remarked:

 #+START_QUOTE
 It feels like a home to the oriented observer.  Something about it allows for a kind of /identification/ with spectator, who feels as if he could crawl right into the little, natural nook, and watch the rainbow currents right from the ocean floor.  The reds and yellows of the foreground then rightly come to focus as our tropical schools of fish. 
 #+END_QUOTE 

 The original paper was seminal, and Pinkston's claim that the patch contained such sophisticated representation in the fish is widely considered one the catalysts to the representationist movement in the patchloop analysis of the mid-eighties. 

 But, and this is not talked about as much, the 8 loop analysis was challenged almost immediately by Girschill (1983).  She argued that Pinkston's "blue" bar is in fact a more generalized "subjective indentation in the manifest background":

 #+START_QUOTE
 While Pinkston was right that our "bar" here is clearly associated with the observers own subjectivity, the meaning is more sophisticated than either indentification or as a kind of "home" (disregarding the fact those two qualities would seem to contradict each other!).  We observe the supposed left edge of our bar, X6, is not so different from its west neighbor, who in turn is not so different from /its/ west neighbor.  The bar is clearly in itself a gradient that begins at X5, or even X4.  The naive, masculine reading here that identification in consistency equates to a "nook" to claim as your own should be challenged by the gradient.  I read here something more tactile, interactive...
 #+END_QUOTE   

 We prefer to read these two together: the bar rests on the edge of comfort and action.  It reminds us that we can find ourselves close to ourselves in the unlikeliest of places, and at that same time, realize that it is most often our own actions that bring us this comfort.    
** DONE green orange blue
CLOSED: [2019-12-24 Tue 16:17]
*** id
 p8/13E/&21
*** analysis
    This is a _type 2 hybrid patch_, meaning it is composed of type 2 (dirty) loops and a few type 1 loops.  It is primarily characterized by a 6-loop sequence on the horizontal set, with the vertical set loops being generally darker, implying a functional background to the patch.  This is a rare hybrid form on two counts: (1) there are two type 1 colors here, 3 green and 1 orange; and (2) we find an example of _type agnosticism_ in the main sequence, meaning the orange loop serves as an orange token in the main, horizontal-set sequence, with the other two orange tokens being dirty loops.
   
    The principle three 6-loop sequence on the horizontal set makes this an early asymmetrical patch.  From top to bottom the sequence runs: bright green, light blue/gray, orange, dark green/black, yellow/black, black/gray.  This runs completely through six times, ending with the final black at the bottom.  Notably, the upper type 1 green loop is mostly obfuscated by the north edge.
   
*** interpretation

 This patches traffics among concepts of completion and balance, or rather, completion and /imbalance/.  We sense a kind of uncanny intentionality in the iteration of the main horizontal sequence.  It completes successfully three times in the patch, but does not produce any kind of organic satisfaction: it is neither a symmetrical sequence, and it is a visually awkward sequence of 6 in our patch domain of 18.  This, for many of us, is reminiscent of a kind of daily life.  Hard repeating sequences of disparate shades, set against each other impersonally reminds us of the alienating labor of capitalism, as each working day is the same set sequence of shades, unceremoniously set alongside each other throughout a week.  

 In this reading, our sampling here of three same-days is interesting, especially as three is typically coordinated along with concepts of strength and perseverance elsewhere (ABD34, X5FG6, LLL5D4).  Further, the type-1 loop placements here remind us that every day is a chance for something nice to happen for a minute or hour or two.    

** DONE red and black
   CLOSED: [2019-12-20 Fri 16:47]
*** id
 p8/234/2
*** analysis
 A pure type-2 patch, composed of almost exclusively dark reds and blacks, although we find pinks and blues as well.  This is often considered one of the "early checkerboxes," with a clear, primary color in the horizontal loops and dark to black background in the vertical set.  

 Arguably the most striking visually feature of ~234/2~ is the blue vertical line on x10 which interrupts the black loops of the vertical set and threatens the coherency of the overall checkerbox.   

*** interpretation
 Red and black in patchloops are typically considered to symbolize issues of the body and the practical activities of day to day life.  But, as the analysis notes, there are many patches with a similar checkerbox template, with black always taking up the verticals (cf. p8/0BE/2 and p8/124/&21). This has led some commentators to diminish the combination of red and black, and focus on the red.  Hence Peter Shelman's famous opus /Ted and the Red Checkerbox/.  

** DONE mountain
CLOSED: [2019-12-24 Tue 16:30]
*** id
    p8/2A7/&21
*** analysis
    2A7 is a classic type 2 hybrid looppatch, with a rather complex gradient in horizontal set, and a collection of light blues and yellows in the vertical.  Its hybrid designation is somewhat rare on the count of there being only one exception to the type 2 set of loops composing the patch: a green type one loop on Y3.  

    Classic commentary on this patch typically revolves around the somewhat natural, organic colors that compose it. The horizontal set moves from dark greens to bright greens, then from maroons and purples to dark blues and a final (Y18) black loop.  The vertical set is more straightforward, but its atmospheric colors is largely argued to add as much to the general natural representation of the patchloop.
*** interpretation
Among the many existential ramifications of imminent climate disaster is a kind of surreal familiarity with the earth itself.  In this terrestrial irony, our own neglect and chauvinism has done nothing but implicate ourselves into the earth.  This new situation, the "anthropocene" as it has been called, creates (by unintuitive logic) a situation where humanities collective ignorance has provided it with planetary intimacy.  

The planet is us, and we are the planet, in the worst possible way.  The overlay of sky and rusted-mountain on 2A7, reminds the subject of the early anthropocenic epoch that place and history are intimately tied together: we are above and below, among and without.  The future is the screen of this show, but we are not the audience.    
** DONE orange and black and yellow
   CLOSED: [2019-12-26 Thu 12:37]
*** id
       p8/2E5/&21
*** analysis  
2E5 is a type 2 hybrid patchloop composed primarily of orange, black, and yellow type 2 loops. While a 2-1, and not 1-2 hybrid, 2E5 is heavily characterized by a collection of type-1 orange loops that form an inner square, its second-most significant feature. 

The most significant feature of 2E5, at least in general consensus of modern patchloop analysis (Rickchards 2013, Jacoby 2009), is the intersection of yellow-gold loops at X11Y7, and the twice-over intersection of black loops in the upper right quadrant (X14Y4 and X15Y5).  This kind of coordination and mirroring of horizontal and vertical set loops is not typically seen outside of higher concept patches of this era (p8/2G6, p8/3ER). 
*** interpretation
Patchloop 2E5 is famously recreated in Harold Coodises' Opera /Goldey Coldey/. The show, which Coodises described as "a confession of tastelessness, followed by a defense of trash," is a large production focused on the locales of a certain unnamed-city block, which takes up the place of the inner orange square our analysis mentions. 

The story begins at the "7-11 gold store", where our hero, Jocelyn Gate, works.  It takes the order and intention implied by the intersection of yellow loops in 2E5 and reimagines it as a kind of Kafkaesque pharmacy, where patrons pay to name the afflictions they come in with, and leave with nothing but the name.  Around the corner of our block seeps the dark tendrils of the state house, a large black cube that seeks to iterate the city to nothingness. As the story progresses, Jocelyn learns of the nefarious black cube (our X14Y4/X15Y5 intersections in the patch), and in this learns of her own alienation.  

We take the geographical/urban-environmental spin Coodises projects onto 2E5, and we note that we are only ever as strong as our own community, and this truth in turn teaches us that hope can be shared and borrowed. 


** DONE nib, white, blue
   CLOSED: [2019-12-26 Thu 12:37]
*** id  
    (4-1-2)
    p8/14G/2
*** analysis
A pure type 2 patch composed of light blues, dark blues, and yellows.  The rather straightforward pallet is fairly uncharacteristic of a non-hybrid type 2, placing this patch firmly in the "middle" period of type 2 patches.  The most significant feature of the patch is the rare "nib" on its primary face: a small knot on loop X10 protrudes from the main face, creating the famous "14G nib." 

While the nib on 14G broadly singularizes it among the canon, it is variously grouped among several patch genres.  Most popularly, it is grouped with patches such as 2A7 ("Mountain") and 3EL, namely the "type 2 representatives" (Schieling 2014).  Some newer schools regard this among the "early ombré patches," respectively grouping it with the 1G0 and 214.    
*** interpretation
This patch deals with concepts of subjectivity and place.  Personality, as we know, is the mere sedimentation of stimuli along a semi-permeable, semi-resistant membrane.  As rocks split and smooth in the river, so does the styled ego of the individual from the stream of impressions the world provides.

Among the many implications of this concept of the individual is a newfound appreciation for precious moment of introspection.  A life dominated by repitition and humdrum is void of introspective access for the simple reason that it is only when things change can one really separate themselves for their surroundings. Only when life alters can we individuate the individual, and ask questions of them. 

This is the general lesson of what commentators call the "nib" of 14G.  The new dimensionality the nib finds among the universe of aesthetic patchloop-features, performs a kind psychological story: its relief places the subject, and interrogates them.  It reminds us that we are in fact not strangers to ourselves, and that confidence comes from clearing away internal opacity, not disregarding others.  
** DONE type 1 w/ purple
CLOSED: [2019-12-28 Sat 11:39]
*** person
    larndz
*** id
    8-3-9
    p8/2AB/1
*** analysis
   One of three known "pure" type 1 patches.  This type classification alone distinguishes 2AB, and pairs it with the other commonly circulated pure type 1 loop patch, p8/6H.  
    The horizontal set presents with a banal pattern of stark, neon colors, while the vertical set retains 16 purple loops with two white loops on X1 and X18.       
    Using Corey's method (1993) for parsing the pattern in the horizontal set, the resulting matrix is
    X00XXX00XX00XXX00X
    0X00000X00X00000X0
    00X000X0000X000X00
*** interpretation 
Baker Corey is one of the more recognizable names in modern patch analysis. Her work in various methods of patch serialization created a wave of new literature in early patchloops, for better and worse.   

A less well known theory Corey cultivated, one that she once noted as the "the proper goal for all patch serialization efforts" (/Love Loops/, 1999), is the concept of "rhythm" in patches.  Corey thought of this effort as a kind of makeshift Fourier transformation of patch loops, decomposing the immanent, totalizing /signal/ of patches into constitutive "rhythms", expressing a radical temporality to early vertical-static patches.

Corey serialization in 2AB draws a kind of cascading series of accents, radiating around a tunnel.  We read the spirit of Corey's work in this patch: as differentiation is registered in the time domain, so one can find it in various constitutive frequencies.  As above as below.  Consider how your life and body are two manifestations of one ultimately anonymous experience, and that there is always two ways to say the same thing.
** DONE type 1 w/ white
CLOSED: [2019-12-28 Sat 12:58]
*** id
    p8/6H/1
*** analysis
Along with, 2AB, 6H is the other main "pure" type 1 patch of the second canon.  Compared to 2AB, 6H's pallet is relatively restrained, with only three distinctive colors. Like of most patches of this era, the horizontal set is functional background (here, white); the vertical set loops give a simple, alternating yellow and blue pattern.

While its restrained pallet is generally a feature of type 1 dominated or pure patches, 6H is sometimes grouped with "the symbolic array" of patches first named by Goipin in 1980, and generally defined by their  palletes of three. 
*** interpretation
6H is a patch of youth, home, and meditation.  The yellow and blue foregrounding colors are reminiscent of p8/2A7 (the famous "Mountain" patch), stripped of their organic background, and applied to the stark, minimized background of a domestic room.  

That is, moving from 2A7 to 6H is moving from a deterritorialized, arboreal landscape, to a bedroom, only sparsely decorated.  The dappled drywall across from you paints streaming blue and yellow lines, borrowed in constrained ways from the outside world.  

All the sounds of the day ring in a dull fade as your little toy sky presents itself on the wall, and your breath becomes more implicated in your thoughts.  You consider the way your fingers seem to recreate your thoughts as they move around in your hand.  You chuckle.  You are at home.   
** DONE candy 
   CLOSED: [2019-12-28 Sat 13:34]
*** id
    7-7-1
    p8/26F/2
*** analysis 
26F is known as the "candy" patch and it is a mature non-oriented patch of the second canon.  Non-orientation patches are defined by the agnosticism of function between the vertical and horizontal set of loops in creating a visual representation, and typically are considered "emotive" in this.

The "candy patch" nickname comes from Pinkston and Corey's 1986 paper on emotive patches, and is "understandably" one of the more popular patches in the kitchen revivalist movement of patchloops (Crikmon 2017, Calley 2018). 

Both sets are dominated by a wide spectrum of pinks, blues, and purples.
*** interpretation  
Commentator's can get quite passionate talking about some of these second-canon emotive patches; largely, in this committee's opinion, to a fault.  Emotive patches should be moments of intellectual reprieve, and associative play, not heady ventures into crypto-Freudianism.   

In this, we take /candy/ as it is:  a pile of goop, held together by will and potential, ever renewing itself as a simple affirmation of affection itself.

When Cartlin gave his famous speech at 1988's Loop Conference in San Francisco, he ended with a short anecdote on 26F:

"I remember waking up at my cousins house in Arvada in 1956, putting my boots on to go see the sky.  Walking past my open journal in the desk of the guest room, I remember seeing the sketch of /candy/, and just stopping in my tracks: how long had it took me to actually see it?  Because it was right in that moment    

** DONE quad orange blue
   CLOSED: [2019-12-28 Sat 14:11]
*** id
    p8/196/&21
*** analysis 
p8/196 is a classic "quad" type 2 hybrid patch, exclusively composed of blues and oranges.  Like virtually all quad-types, a properly oriented patch will have two solid 8x8 squares, one on the bottom right and another top left.  For 196, this means a solid orange box bottom right, and blue upper left.    

Because both the horizontal and vertical sets take part in the conceptual framework of quad patches, and because they have limited palletes by necessity, many commentators call them "emotive by default" and group them accordingly.  

Some patches in horizontal set are more red than orange, but few have considered this to trouble the overall consistency or “quadness” of the patch.
*** interpretation
    Quad patches emphasize concepts of psychological taxonomy and health.  196 is a fiery exercise of concept and projection.  
    A common anxiety in the early 21st century pertains to issues of legitimacy in beliefs and thoughts.  Namely, it is /not/
** TODO red blue black pure type two
*** id
    p8/240/&21
** TODO gradient pink black 
*** person
    noel
*** id
    p8/214/&21
*** analysis 
    214 is a clear, relatively no frills ombré patch, 
** TODO green type 3
*** id 
   p8/109/3 
** TODO gradient white blue
*** person
    abbie
*** id
    p8/1G0/2
** TODO black and yellow checker
*** id
    p8/32/&21
    
