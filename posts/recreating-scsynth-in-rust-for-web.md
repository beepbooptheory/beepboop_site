---
title: A learning adventure with Supercollider, Rust, and the web
description: Introductory post on a project for the web
date: 2021-08-16
---

In attempt to save me from destituion, I have decided to use focus and conjur
something with all my powers. This will be an experiment of somewhat large
proportions, relative to me, and will attempt to make something that I alone
would probably _not want to do_, but at the same time, something that aligns
with my interests and could maybe be used by others/help me get a job.

We will take some pieces we have found laying around, and try to fit them together.

## Piece number 1: The weird and terrible world of modern Web Audio API

If you want to work with audio in a web browser, it would seem that you have a
wonderful thing to work with: the [Web Audio
API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API) is a rich
thing, with a node architecture that contains various common processors for
sound-type data.

The MDN article linked above has some good reminders we will use as prayers 
throughout this project: 

> In fact, sound files are just recordings of sound intensities themselves, which come in from microphones or electric instruments, and get mixed down into a single, complicated wave.

## Piece number 2: Supercollider, specifically, the client -> server architecture of sclang->scsynth

Despite its overwhelming power to control the way we use computers, some things
can still escape the capitalist mindset of our systems. That is, if your
interactions with coding/software development is initially or exclusively one
that is discussing "creative coding," at some point you start to get a disctinct
sense that there are number of things you need to work around, or even simply
there is so much around you that is "not for you". 

Consider the humble `git` and not-so-humble GitHub, which are at once truly
universal in the world of software development and yet, to use coding parlance,
quite opinionated about these tools. The idea of version-control itself is (or
at least, seems) pure: whether you use a scripting/dynamic library, or a
compiled one, much of the work of making software is by necessity iterative.
Depending on the language you are in, you need to implement an X before you can
manifest a Y. In javascript, you need to initialize an array before you can push
to it, even if you have already defined the variable you can find that array in.
That is a simple example, but this simplicity is mirrored at all the higher
scopes of the problem you can consider. You want to make an API? You better have
a pretty good type system implemented which can assure that people can expect
the right data back. You want to build a command line application? Well, you
better make sure you can validate a user's input.


That is to say, like most art, coding is a game of preparation and context.

based of the concerns of stakeholders and "users" (read: "money").
The labor of coding is extreme on some levels,
