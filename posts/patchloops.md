---
title: Patchloops
date: 2021-08-09
---

##  p8/0BE/2 ("blue and white")

### analysis

A type 2 patch, arguably characterized by a distinct horizontal set foreground of darker blues.  The vertical set here is lighter, almost pastel tones with a large section of sequential blues spanning X6 to X14.  The foreground is composed of rich, marine blues, reds, greens, and yellows, in diminishing frequency respectively.  

This patch is typically paired with ####, or the &ldquo;red and black&rdquo; checkerbox.  But while red and black creates a more consistent and even monochrome, this patch is rather successful in the opposite way: a sense of a balanced and clear checkerbox is brought about at almost the limit of granularity.  The biggest threat to the overall checkerbox gestalt is the two vertical pink loops at X2 and X16. 

### Interpretation

As reflected in the above analysis, the large consensus concerning the understated sequence of blue, vertical loops in this patch goes that it runs exactly 8 loops, X6 to X14, but this has in fact been under debate for a long time.  Pinkston (1983) was the first to identify the sequence, affectionality referring to it as &ldquo;the blue bar&rdquo;.  He remarked:

> It feels like a home to the oriented observer.  Something about it allows for a kind of *identification* with spectator, who feels as if he could crawl right into the little, natural nook, and watch the rainbow currents right from the ocean floor.  The reds and yellows of the foreground then rightly come to focus as our tropical schools of fish.

The original paper was seminal, and Pinkston&rsquo;s claim that the patch contained such sophisticated representation in the fish is widely considered one the catalysts to the representationist movement in the patchloop analysis of the mid-eighties. 

But, and this is not talked about as much, the 8 loop analysis was challenged almost immediately by Girschill (1983).  She argued that Pinkston&rsquo;s &ldquo;blue&rdquo; bar is in fact a more generalized &ldquo;subjective indentation in the manifest background&rdquo;:

>While Pinkston was right that our &ldquo;bar&rdquo; here is clearly associated with the observers own subjectivity, the meaning is more sophisticated than either indentification or as a kind of &ldquo;home&rdquo; (disregarding the fact those two qualities would seem to contradict each other!).  We observe the supposed left edge of our bar, X6, is not so different from its west neighbor, who in turn is not so different from *its* west neighbor.  The bar is clearly in itself a gradient that begins at X5, or even X4.  The naive, masculine reading here that identification in consistency equates to a &ldquo;nook&rdquo; to claim as your own should be challenged by the gradient.  I read here something more tactile, interactive&#x2026;

We prefer to read these two together: the bar rests on the edge of comfort and action.  It reminds us that we can find ourselves close to ourselves in the unlikeliest of places, and at that same time, realize that it is most often our own actions that bring us this comfort.    

## green orange blue

### id

p8/13E/&21

### analysis

This is a <span class="underline">type 2 hybrid patch</span>, meaning it is composed of type 2 (dirty) loops and a few type 1 loops.  It is primarily characterized by a 6-loop sequence on the horizontal set, with the vertical set loops being generally darker, implying a functional background to the patch.  This is a rare hybrid form on two counts: (1) there are two type 1 colors here, 3 green and 1 orange; and (2) we find an example of <span class="underline">type agnosticism</span> in the main sequence, meaning the orange loop serves as an orange token in the main, horizontal-set sequence, with the other two orange tokens being dirty loops.

The principle three 6-loop sequence on the horizontal set makes this an early asymmetrical patch.  From top to bottom the sequence runs: bright green, light blue/gray, orange, dark green/black, yellow/black, black/gray.  This runs completely through six times, ending with the final black at the bottom.  Notably, the upper type 1 green loop is mostly obfuscated by the north edge.

#### interpretation

This patches traffics among concepts of completion and balance, or rather, completion and *imbalance*.  We sense a kind of uncanny intentionality in the iteration of the main horizontal sequence.  It completes successfully three times in the patch, but does not produce any kind of organic satisfaction: it is neither a symmetrical sequence, and it is a visually awkward sequence of 6 in our patch domain of 18.  This, for many of us, is reminiscent of a kind of daily life.  Hard repeating sequences of disparate shades, set against each other impersonally reminds us of the alienating labor of capitalism, as each working day is the same set sequence of shades, unceremoniously set alongside each other throughout a week.  

In this reading, our sampling here of three same-days is interesting, especially as three is typically coordinated along with concepts of strength and perseverance elsewhere (ABD34, X5FG6, LLL5D4).  Further, the type-1 loop placements here remind us that every day is a chance for something nice to happen for a minute or hour or two.

### p8/234/2

#### p8/234/2: analysis

A pure type-2 patch, composed of almost exclusively dark reds and blacks, although we find pinks and blues as well.  This is often considered one of the &ldquo;early checkerboxes,&rdquo; with a clear, primary color in the horizontal loops and dark to black background in the vertical set.  

Arguably the most striking visually feature of `234/2` is the blue vertical line on x10 which interrupts the black loops of the vertical set and threatens the coherency of the overall checkerbox.   

### p8/234/2: interpretation

Red and black in patchloops are typically considered to symbolize issues of the body and the practical activities of day to day life.  But, as the analysis notes, there are many patches with a similar checkerbox template, with black always taking up the verticals (cf. p8/0BE/2 and p8/124/&21). This has led some commentators to diminish the combination of red and black, and focus on the red.  Hence Peter Shelman&rsquo;s famous opus *Ted and the Red Checkerbox*.  

## p8/2A7/&21

### p8/2A7/&21 analysis

2A7 is a classic type 2 hybrid looppatch, with a rather complex gradient in horizontal set, and a collection of light blues and yellows in the vertical.  Its hybrid designation is somewhat rare on the count of there being only one exception to the type 2 set of loops composing the patch: a green type one loop on Y3.  

Classic commentary on this patch typically revolves around the somewhat natural, organic colors that compose it. The horizontal set moves from dark greens to bright greens, then from maroons and purples to dark blues and a final (Y18) black loop.  The vertical set is more straightforward, but its atmospheric colors is largely argued to add as much to the general natural representation of the patchloop.

### p8/2A7/&21: interpretation

Among the many existential ramifications of imminent climate disaster is a kind of surreal familiarity with the earth itself.  In this terrestrial irony, our own neglect and chauvinism has done nothing but implicate ourselves into the earth.  This new situation, the &ldquo;anthropocene&rdquo; as it has been called, creates (by unintuitive logic) a situation where humanities collective ignorance has provided it with planetary intimacy.  

The planet is us, and we are the planet, in the worst possible way.  The overlay of sky and rusted-mountain on 2A7, reminds the subject of the early anthropocenic epoch that place and history are intimately tied together: we are above and below, among and without.  The future is the screen of this show, but we are not the audience.    


<a id="org7fcecf6"></a>

## p8/2E5/&21

### p8/2E5/&21: analysis

2E5 is a type 2 hybrid patchloop composed primarily of orange, black, and yellow type 2 loops. While a 2-1, and not 1-2 hybrid, 2E5 is heavily characterized by a collection of type-1 orange loops that form an inner square, its second-most significant feature. 

The most significant feature of 2E5, at least in general consensus of modern patchloop analysis (Rickchards 2013, Jacoby 2009), is the intersection of yellow-gold loops at X11Y7, and the twice-over intersection of black loops in the upper right quadrant (X14Y4 and X15Y5).  This kind of coordination and mirroring of horizontal and vertical set loops is not typically seen outside of higher concept patches of this era (p8/2G6, p8/3ER). 

### p8/2E5/&21: interpretation

Patchloop 2E5 is famously recreated in Harold Coodises&rsquo; Opera *Goldey Coldey*. The show, which Coodises described as &ldquo;a confession of tastelessness, followed by a defense of trash,&rdquo; is a large production focused on the locales of a certain unnamed-city block, which takes up the place of the inner orange square our analysis mentions. 

The story begins at the &ldquo;7-11 gold store&rdquo;, where our hero, Jocelyn Gate, works.  It takes the order and intention implied by the intersection of yellow loops in 2E5 and reimagines it as a kind of Kafkaesque pharmacy, where patrons pay to name the afflictions they come in with, and leave with nothing but the name.  Around the corner of our block seeps the dark tendrils of the state house, a large black cube that seeks to iterate the city to nothingness. As the story progresses, Jocelyn learns of the nefarious black cube (our X14Y4/X15Y5 intersections in the patch), and in this learns of her own alienation.

We take the geographical/urban-environmental spin Coodises projects onto 2E5, and we note that we are only ever as strong as our own community, and this truth in turn teaches us that hope can be shared and borrowed.

## p8/14G/2 (nib, white, blue)
